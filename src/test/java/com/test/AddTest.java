package com.test;

import com.test.calculator.Add;
import com.test.calculator.AddImpl;
import org.junit.Before;
import org.junit.Test;

import static org.easymock.EasyMock.createNiceMock;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.replay;
import static org.easymock.EasyMock.verify;
import static org.junit.Assert.assertEquals;

public class AddTest {
    Add add;

    @Before
    public void setUp(){
        add = createNiceMock(AddImpl.class);
    }

    @Test
    public void testAdd(){
        expect(add.add(3,2)).andReturn(5);
        replay(add);

        assertEquals(5, add.add(3, 2));
        verify(add);
    }
}
